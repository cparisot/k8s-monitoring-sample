### Procédure d'installation du monitoring

# Install depuis coreos repo
## 1 Initialisation
from https://github.com/coreos/prometheus-operator
```
git clone https://github.com/coreos/prometheus-operator.git
```

## 2 Installation
be sure to be admin on cluster
```
  kubectl create clusterrolebinding cluster-admin-binding-CYPA --clusterrole=cluster-admin --user=$(gcloud config get-value core/account)
```
lancer l'installation
```
cd contrib/kube-prometheus/
hack/cluster-monitoring/deploy
```

Edit passer en http pour métriques du kubelet **(droit GKE)**

*changer :
   port: https-metrics
 scheme: https
en :
   port: http-metrics
 scheme: http*
```
 deploy.sh
```

## 3 Acces
test prometheus
```
kubectl port-forward -n monitoring prometheus-k8s-0 9090
```
go http://127.0.0.1:9090/

test grafana
```
kubectl port-forward $(kubectl get  pods --selector=app=grafana -n monitoring --output=jsonpath="{.items..metadata.name}") -n monitoring 3000
```
go http://127.0.0.1:3000/

## 4 Procédure de nettoyage de la stack

```
kubectl delete ns monitoring
```
