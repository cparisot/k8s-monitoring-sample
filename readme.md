# Sample of monitoring (Prometheus) installation

# Requirements for GCP
- Check if loggingService and monitoring services are configure

 `gcloud container clusters describe --zone=europe-west3-b gke-cluster-build  | grep "monitoringService\|loggingService"to check
loggingService: logging.googleapis.com/kubernetes
monitoringService: monitoring.googleapis.com/kubernetes`

- Add Clusterrole Rights for Installations
```
ACCOUNT=$(gcloud info --format='value(config.account)')
kubectl create clusterrolebinding owner-cluster-admin-binding \
              --clusterrole cluster-admin \
              --user $ACCOUNT
```
# Prometheus
## Manual installation
 - Create ns
 `kubectl create namespace monitoring`

 - Added permission for you to retrieve kubernetes metrics
 `kubectl apply -f prom-yaml/clusterRoleProm.yaml`

 - Add the prom config in a config map (load your custom rules)
 `kubectl apply -f prom-yaml/config-map.yaml`

 - Prometheus deployment using docker hub image
 `kubectl apply -f prom-yaml/deploy.yaml --namespace=monitoring`

 - Test it
 `kubectl get deployments --namespace=monitoring`

 - Create a service
`kubectl apply -f prom-yaml/service.yaml`


## Installation using CRD *(Recommended)*
Installation of Prometheus, Alerte manager and Grafana

 - Prometheus install :
 ```
 git clone  https://github.com/coreos/prometheus-operator.git`
 kubectl apply -f bundle.yaml
 kubectl apply -f  contrib/kube-prometheus/manifests/  --recursive
 ```
 - Need hack: change basic install to acces kublet/metrics
 `../hack/deploy.sh`

## Acces to graphic interface
- Prometheus
 ```
 kubectl port-forward -n monitoring prometheus-k8s-0 9090
 ```
 Then go http://127.0.0.1:9090/

- Grafana
 ```
 kubectl port-forward $(kubectl get  pods  --selector=app=grafana -n monitoring  --output=jsonpath="{.items..metadata.name}" ) -n monitoring 3000
 ```
 Then go http://127.0.0.1:3000/


# Integration with stackdriver

- **WARN** Not compatible with grafana installation
- **WARN2** Beware of version compatibility,
check it on: https://github.com/Stackdriver/stackdriver-prometheus-sidecar#compatibility

## Stackdriver sidecar install:
```
git clone https://github.com/Stackdriver/stackdriver-prometheus-sidecar.git
export KUBE_NAMESPACE=monitoring
export KUBE_CLUSTER=gke-cluster-build
export GCP_REGION=europe-west3
export GCP_PROJECT=fml-build-infracore
export SIDECAR_IMAGE_TAG=0.4.1
./stackdriver-prometheus-sidecar/kube/full/deploy.sh
```

# Clean stack

 To delete all items delete the namespace, it will cascade delete all linked object
 ```
 kubectl delete ns monitoring
 ```
